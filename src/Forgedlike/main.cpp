#include <iostream>
#include "common.hpp"
#include "Item.hpp"
#include "ItemData.hpp"
#include "ScriptableAttributes.hpp"
#include "ScriptEngine.hpp"
#include "Campaign.hpp"

int main(int argc, char** argv)
{
	ScriptEngine s;
	ItemData::Register(s);
	Item::Register(s);
	ScriptableAttributes::Register(s);
	Campaign c(argv[0], s);
	c.selectCampaign(0);
	c.loadCampaign();
	return 0;
}

