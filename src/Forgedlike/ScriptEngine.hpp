#ifndef SCRIPTENGINE_HPP
#define SCRIPTENGINE_HPP

#include <PimplMacro/PimplMacro.hpp>
#include <string>

namespace chaiscript{
class ChaiScript;
}

class ScriptEngine{
public:
	ScriptEngine();
	~ScriptEngine();

	void            log(std::string const& message);
	void            exec(std::string const& command);
	void            loadFile(std::string const& path);
	chaiscript::ChaiScript& getEngine();
private:
	HAS_PRIVATE_VARIABLES;
};

#endif

