#include "Campaign.hpp"

#include "CampaignMediator.hpp"
#include "ScriptEngine.hpp"
#include <libtcod/libtcod.hpp>
#include <iostream>

PRIVATE_VARIABLES(Campaign){
	PrivateVariables(string const& exe_path, ScriptEngine& SE)
		: SE(SE)
	{
		path = exe_path.substr(0, exe_path.find_last_of("\\/"));
		path += "/data/campaigns/";
	}

	ScriptEngine&   SE;
	string          path;
	vector<string>  campaign_files;
	string          current_campaign;
	string          id;
	string          target;
};

Campaign::Campaign(string const& path, ScriptEngine& SE)
	: INIT_PRIVATE_VARIABLES(path, SE)
{
	std::cout << "Reading Campaign files in " << m->path << std::endl;
	TCODList<const char*> files = TCODSystem::getDirectoryContent(m->path.c_str(), "*.camp");
	for( auto file = files.begin(); file != files.end(); ++file ){
		m->campaign_files.push_back(m->path + *file);
	}
	files.clearAndDelete();

	CampaignMediator::Register(SE, *this);

}

Campaign::~Campaign(){}

void Campaign::selectCampaign(unsigned id)
{
	if( id > m->campaign_files.size() )
		throw runtime_error("Invalid Campaign Selected");
	m->current_campaign = m->campaign_files[id];
}

void Campaign::loadCampaign()
{
	if( m->current_campaign.empty() )
		throw runtime_error("No campaign has been selected");
	m->SE.loadFile(m->current_campaign);
	TCODList<const char*> mod_files = TCODSystem::getDirectoryContent(m->path.c_str(), "*.mod");
	for( auto file = mod_files.begin(); file != mod_files.end(); ++file ){
		m->SE.loadFile(m->path + *file);
	}
	mod_files.clearAndDelete();
}

void Campaign::setId(string const& id)
{
	m->id = id;
}

void Campaign::setTarget(string const& target)
{
	m->target = target;
}

bool Campaign::authMod()
{
	auto id = m->id; auto target = m->target;
	if( id.empty() || target.empty() ) return false;
	return id == target;
}

