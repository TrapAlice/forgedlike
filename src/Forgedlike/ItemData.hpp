#ifndef ITEMDATA_HPP
#define ITEMDATA_HPP

#include "common.hpp"
#include "ScriptableAttributes.hpp"

class ScriptEngine;

struct ItemData{
	ItemData();
	ItemData(ItemData const&);

	ScriptableAttributes attr;

	static void     Register(ScriptEngine& SE);
};

class ItemDataStore{
public:
	static void     Add(string const& id, ItemData const&);
	static ItemData const& Get(string const& id);
private:
	static map<string, ItemData> mItemData;
};

#endif

