#include "CampaignMediator.hpp"

#include "Campaign.hpp"
#include "ScriptEngine.hpp"
#include <chaiscript/chaiscript.hpp>

void CampaignMediator::Register(ScriptEngine& SE, Campaign& c)
{
	using namespace chaiscript;
	SE.getEngine().add(fun(&Campaign::setId, &c), "Id");
	SE.getEngine().add(fun(&Campaign::setTarget, &c), "Target");
	SE.getEngine().add(fun(&Campaign::authMod, &c), "Auth");
}

