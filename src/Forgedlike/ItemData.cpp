#include "ItemData.hpp"

#include "ItemDataMediator.hpp"

ItemData::ItemData()
{}

ItemData::ItemData(ItemData const& i)
	: attr(i.attr)
{}

void ItemData::Register(ScriptEngine& SE)
{
	ItemDataMediator::Register(SE);
}

void ItemDataStore::Add(string const& id, ItemData const& data)
{
	mItemData.insert(std::make_pair(id, data));
}

ItemData const& ItemDataStore::Get(string const& id)
{
	try{
		return mItemData.at(id);
	} catch ( std::out_of_range ){
		throw runtime_error("Tried to create item with id " + id);
	}
}

map<string, ItemData> ItemDataStore::mItemData;

