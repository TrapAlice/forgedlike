#ifndef CAMPAIGN_HPP
#define CAMPAIGN_HPP

#include "common.hpp"
#include <PimplMacro/PimplMacro.hpp>

class ScriptEngine;

class Campaign{
public:
	Campaign(string const& path, ScriptEngine& SE);
	~Campaign();

	void            selectCampaign(unsigned id);
	void            loadCampaign();
	void            setId(string const&);
	void            setTarget(string const&);
	bool            authMod();
private:
	HAS_PRIVATE_VARIABLES;
};

#endif

