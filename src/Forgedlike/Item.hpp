#ifndef ITEM_HPP
#define ITEM_HPP

#include "ScriptableAttributes.hpp"
#include <PimplMacro/PimplMacro.hpp>

class ItemData;
class ScriptEngine;

class Item{
public:
	Item(ItemData const&);
	Item(Item const&);
	~Item();

	ScriptableAttributes attr;
	ItemData const& getData();

	static void     Register(ScriptEngine& SE);
private:
	HAS_PRIVATE_VARIABLES;
};

#endif

