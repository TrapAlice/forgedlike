#include "Item.hpp"

#include "ItemMediator.hpp"
#include "ItemData.hpp"
#include "ScriptEngine.hpp"

PRIVATE_VARIABLES(Item){
	PrivateVariables(ItemData const& data)
		: data(data)
	{}
	ItemData const& data;
};

Item::Item(ItemData const& data)
	: INIT_PRIVATE_VARIABLES(data)
{}

Item::Item(Item const& i)
	: attr(i.attr)
	, INIT_PRIVATE_VARIABLES(i.m->data)
{}

Item::~Item()
{}

ItemData const& Item::getData()
{
	return m->data;
}

void Item::Register(ScriptEngine& SE)
{
	ItemMediator::Register(SE);
}
