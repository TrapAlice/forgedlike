#include "ItemDataMediator.hpp"

#include "ItemData.hpp"
#include "ScriptEngine.hpp"
#include <chaiscript/chaiscript.hpp>

void ItemDataMediator::Register(ScriptEngine& SE)
{
	using namespace chaiscript;
	SE.getEngine().add(user_type<ItemData>(), "ItemData");
	SE.getEngine().add(constructor<ItemData()>(), "ItemData");
	SE.getEngine().add(constructor<ItemData(ItemData&)>(), "ItemData");
	SE.getEngine().add(fun(&ItemData::attr), "attr");
	SE.exec("def AddItem(id, item)"
	        "{ if( Auth() ){ _AddItem(id, item); }}");
	SE.getEngine().add(fun(&ItemDataStore::Add), "_AddItem");
	SE.getEngine().add(fun(&ItemDataStore::Get), "GetItemData");
}
