#ifndef SCRIPTABLEATTRIBUTES_HPP
#define SCRIPTABLEATTRIBUTES_HPP

#include "common.hpp"
#include <PimplMacro/PimplMacro.hpp>

namespace chaiscript{
class Boxed_Value;
}
typedef chaiscript::Boxed_Value Value;
class ScriptEngine;

class ScriptableAttributes{
public:
	ScriptableAttributes();
	ScriptableAttributes(ScriptableAttributes const&);
	~ScriptableAttributes();
	Value& operator[](string const& key) const;

	static void Register(ScriptEngine& SE);
private:
	HAS_PRIVATE_VARIABLES;
};

#endif

