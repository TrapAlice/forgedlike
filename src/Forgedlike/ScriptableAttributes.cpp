#include "ScriptableAttributes.hpp"

#include "ScriptEngine.hpp"
#include <chaiscript/chaiscript.hpp>
using namespace chaiscript;

PRIVATE_VARIABLES(ScriptableAttributes){
	map<string, Value> attr;
};

ScriptableAttributes::ScriptableAttributes()
	: INIT_PRIVATE_VARIABLES()
{}

ScriptableAttributes::ScriptableAttributes(ScriptableAttributes const& s)
	: INIT_PRIVATE_VARIABLES()
{
	m->attr = s.m->attr;
}

ScriptableAttributes::~ScriptableAttributes()
{}

Value& ScriptableAttributes::operator[](string const& key) const
{
	return m->attr[key];
}

void ScriptableAttributes::Register(ScriptEngine& SE)
{
	SE.getEngine().add(fun(&ScriptableAttributes::operator[]), "[]");
}

