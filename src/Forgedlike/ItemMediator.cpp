#include "ItemMediator.hpp"

#include "Item.hpp"
#include "ItemData.hpp"
#include "ScriptEngine.hpp"
#include <chaiscript/chaiscript.hpp>

void ItemMediator::Register(ScriptEngine& SE)
{
	using namespace chaiscript;
	SE.getEngine().add(user_type<Item>(), "Item");
	SE.getEngine().add(constructor<Item(ItemData const&)>(), "Item");
	SE.getEngine().add(constructor<Item(Item&)>(), "Item");
	SE.getEngine().add(fun(&Item::attr), "attr");
	SE.getEngine().add(fun(&Item::getData), "data");
}
