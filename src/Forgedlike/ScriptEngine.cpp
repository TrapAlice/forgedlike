#include "ScriptEngine.hpp"

#include <chaiscript/chaiscript.hpp>

using namespace chaiscript;
using std::string;

PRIVATE_VARIABLES(ScriptEngine){
	ChaiScript      chai;
	string          current_eval;
};

ScriptEngine::ScriptEngine()
	: INIT_PRIVATE_VARIABLES()
{
	m->chai.add(fun(&ScriptEngine::log, this), "log");
}

ScriptEngine::~ScriptEngine(){}

void ScriptEngine::log(string const& message)
{
	std::cout << "[Chai] " << m->current_eval << ": " << message
	          << std::endl;
}

void ScriptEngine::exec(string const& command)
{
	m->current_eval = "EXEC";
	m->chai.eval(command);
}

void ScriptEngine::loadFile(string const& path)
{
	m->current_eval = path.substr(path.find_last_of("\\/")+1, path.size());
	m->chai.eval_file(path);
}

ChaiScript& ScriptEngine::getEngine()
{
	return m->chai;
}

