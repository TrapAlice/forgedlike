#ifndef CAMPAIGN_MEDIATOR_HPP
#define CAMPAIGN_MEDIATOR_HPP

class Campaign;
class ScriptEngine;

class CampaignMediator{
public:
	static void     Register(ScriptEngine&, Campaign&);
};

#endif

